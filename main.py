import argparse
import time
import datetime

from os import system, name
from time import sleep

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('-m','--minute',help="The number of minutes should last.",type=int)
	parser.add_argument('-s','--second',help="The number of seconds should last.",type=int)

	args = parser.parse_args()

	minutes = args.minute if args.minute is not None and args.minute > 0 else 0
	seconds = args.second if args.second is not None and args.second > 0 else 0

	run_timer(seconds=seconds, minutes=minutes)

def clear_console():
	   # for windows 
    if name == 'nt': 
        _ = system('cls') 
  
    # for mac and linux(here, os.name is 'posix') 
    else: 
        _ = system('clear') 

def run_timer(seconds=0, minutes=1):
	total_seconds = minutes * 60 + seconds
	total_time_str = str(datetime.timedelta(seconds=total_seconds))
	elapsed_time = 0
	starting_time = time.time()


	print("Timer starts")
	while elapsed_time < total_seconds :
		elapsed_time = time.time() - starting_time
		remaining_seconds = int(total_seconds - elapsed_time)
		remaining_time_str = str(datetime.timedelta(seconds=remaining_seconds))

		sleep(1)
		clear_console()
		print("remaining time: " + remaining_time_str)

	print("Timer done")
if __name__ == "__main__" :
	main()